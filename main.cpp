#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "VDinamico.h"
#include <set>
#include "UTM.h"
#include "Aeropuerto.h"

using namespace std;

/**
 * Funcion para leer archivos CSV
 */
VDinamico<Aeropuerto> leerCSV() {
    // Vector a devolver con los datos
    VDinamico<Aeropuerto> vector;

    ifstream is;
    stringstream  columnas;
    string fila;

    // Variables para rellenar con la lectura
    string id;
    string ident;
    string tipo;
    string nombre;
    string latitud_str;
    string longitud_str;
    string continente;
    string iso_pais;
    float latitud, longitud;

    is.open("../aeropuertos.csv");
    if ( is.good() ) {
        while ( getline(is, fila ) ) {
            if (!fila.empty()) {
                columnas.str(fila);

                getline(columnas, id, ';');
                getline(columnas,ident,';');
                getline(columnas,tipo,';');
                getline(columnas,nombre,';');
                getline(columnas,latitud_str,';');
                getline(columnas,longitud_str,';');
                getline(columnas,continente,';');
                getline(columnas,iso_pais,';');

                //  Transformamos la latitud y longitud a float
                latitud=stof(latitud_str);
                longitud=stof(longitud_str);

                // Limpiamos variables
                fila="";
                columnas.clear();

                // Mostramos informacion del aeropuerto leido
//                cout << ++contador
//                     << " Aeropuerto: ( ID=" << id
//                     << " ident=" << ident << " Tipo=" << tipo << " Nombre=" << nombre
//                     << " Posicion=(" << latitud << ", " << longitud << ")"
//                     << " Continente=" << continente << " Pais=" << iso_pais
//                     << ")" << endl;

                //Creamos objeto aeropuerto y lo insertamos en el VDinamico
                UTM posicion = UTM(latitud,longitud);
                Aeropuerto aeropuerto = Aeropuerto(id, ident, tipo, nombre, posicion, continente, iso_pais);
                vector.insertar(aeropuerto);
            }
        }
        is.close();
    } else {
        cout << "Error de apertura en archivo" << endl;
    }
    return vector;
}



/**
 * @author Jaime Rubio López jrl00039@red.ujaen.es
 * @author Dalton Gabriel Abambari Collaguazo dgac0002@red.ujaen.es
 */
int main(int argc, const char * argv[]) {
    cout << "Leyendo fichero CSV..." << endl;
    // Creamos e inicializamos el vector de aeropuertos
    clock_t ini = clock();
    VDinamico<Aeropuerto> aeropuertos = leerCSV();
    cout << "Tiempo lectura CSV: " << ((clock() - ini) / (float) CLOCKS_PER_SEC) << " segs." << endl;

    // Ordena de manera descendente y muestra los primeros 30 aeropuertos
    ini = clock();
    cout << "\nOrdenando de manera descendente..." << endl;
    aeropuertos.ordenarRev();
    cout << "Tiempo en ordenar de manera descendente: " << ((clock() - ini) / (float) CLOCKS_PER_SEC) << " segs." << endl;
    cout << "\nMostrando los primero 30 aeropuertos:" << endl;
    aeropuertos.print(30);

    // Ordena de manera ascendente y muestra los primeros 30 aeropuertos
    ini = clock();
    cout << "\nOrdenando de manera ascendente..." << endl;
    aeropuertos.ordenar();
    cout << "\nTiempo en ordenar de manera ascendente: " << ((clock() - ini) / (float) CLOCKS_PER_SEC) << " segs." << endl;
    cout << "Mostrando los primero 30 aeropuertos:" << endl;
    aeropuertos.print(30);

    // Realizo las busquedas por IDs
    cout << "\n\nComienzo de las busquedas:" << endl;
    ini = clock();
    const set<string> IDs = {"345166", "6640", "6676", "345364", "6778"};
    for (const string &id : IDs) {
        Aeropuerto buscar;
        buscar.setId(id);
        const int pos = aeropuertos.busquedaBin(buscar);
        if (pos != -1) {
            cout << "La posicion en el vector del aeropuerto con id " << id << " es: " << pos << endl;
        } else {
            cout << "El aeropuerto con id 345166 no se ha encontrado: " << endl;
        }
    }
    cout << "Tiempo en realizar todas las busquedas: " << ((clock() - ini) / (float) CLOCKS_PER_SEC) << " segs." << endl;

    // Realizo los borrados
    ini = clock();
    VDinamico<Aeropuerto> borrados;
    cout << "\nEl tamaño logico del vector original antes de borrar es: " << aeropuertos.tamLogico() << endl;
    for (int i=0; i<aeropuertos.tamLogico(); i++) {
        if (aeropuertos[i].getContinente() == "NA") {
            borrados.insertar(aeropuertos[i]);
            aeropuertos.borrar(i);
        }
    }
    cout << "El tamaño logico del vector original despues de borrar es: " << aeropuertos.tamLogico() << endl;
    cout << "El tamaño logico del vector de borrados es: " << borrados.tamLogico() << endl;
    cout << "Tiempo en realizar todos los borrados: " << ((clock() - ini) / (float) CLOCKS_PER_SEC) << " segs." << endl;

    // Mostrar info de los borrados
    cout << "\nMostrando los primero 20 aeropuertos borrados:" << endl;
    borrados.print(20);

    return 0;
}