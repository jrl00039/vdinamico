//
// Created by Dalton Gabriel Abambari on 25/9/23.
//

#ifndef PR1_AEROPUERTO_H
#define PR1_AEROPUERTO_H
#include <string>
#include "UTM.h"

using namespace std;

class Aeropuerto {
private:
    string _id;
    string _ident;
    string _tipo;
    string _nombre;
    UTM _posicion;
    string _continente;
    string _iso_pais;
public:
    Aeropuerto();
    Aeropuerto(const string &id, const string &ident, const string &tipo, const string &nombre,
               const UTM &posicion, const string &continente, const string &isoPais);
    Aeropuerto(const Aeropuerto &orig);


    virtual ~Aeropuerto();

    const string &getId() const;
    void setId(const string &id);

    const string &getIdent() const;
    void setIdent(const string &ident);

    const string &getTipo() const;
    void setTipo(const string &tipo);

    const string &getNombre() const;
    void setNombre(const string &nombre);

    const UTM &getPosicion() const;
    void setPosicion(const UTM &posicion);

    const string &getContinente() const;
    void setContinente(const string &continente);

    const string &getIsoPais() const;
    void setIsoPais(const string &isoPais);

    bool operator<(const Aeropuerto &other) const {
        return _id < other._id;
    }

    bool operator>(const Aeropuerto &other) const {
        return _id > other._id;
    }

    bool operator==(const Aeropuerto &other) const {
        return _id == other._id;
    }

};


#endif //PR1_AEROPUERTO_H
