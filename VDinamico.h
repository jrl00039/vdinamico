//
// Created by Dalton Gabriel Abambari on 25/9/23.
//
#ifndef VECTORDINAMICO_H
#define VECTORDINAMICO_H
#include <iostream>
#include <climits>
#include <new>
#include <algorithm>
#include <cmath>
#include <stdexcept>

using namespace std;

template<class T>
class VDinamico {
public:
    VDinamico();
    explicit VDinamico(unsigned int n);
    VDinamico(const VDinamico<T>& orig);
    VDinamico(const VDinamico<T>& orig, unsigned int posicionInicial, unsigned int numElementos);
    virtual ~VDinamico();

    void insertar(const T& dato, unsigned int pos = UINT_MAX);
    T borrar(unsigned int pos = UINT_MAX);
    void ordenar();
    void ordenarRev();
    int busquedaBin(T& dato);
    unsigned tamLogico();
    void print( int tam );
    VDinamico<T>& operator=(const VDinamico<T>& orig);
    T& operator[](int pos);

private:
    unsigned int tamf; //fijo hasta que sea necesario aumentar
    unsigned int taml; //es el que va cambiando cuando metemos y sacamos datos
    T *vector;
};



/**
 * Constructor por defecto
 * @tparam T
 */
template<class T>
VDinamico<T>::VDinamico() : tamf(1), taml(0) {
    this->vector = new T[1];
}

/**
 * @brief Constructor parametrizado para inicializar el vector con un tamaño dado
 * @param n Tamaño logico inicial del vector
 */
template<class T>
VDinamico<T>::VDinamico(unsigned int n) : taml(n) {
    if (n != 0) {
        unsigned int a = (int) pow(2, ceil(log(taml) / log(2)));
        this->tamf = a;
    } else {
        this->tamf = 1;
    }
    vector = new T[this->tamf];
}

/**
 * @brief Constructor copia
 * @param orig VDinamico original a copiar
 */
template<class T>
VDinamico<T>::VDinamico(const VDinamico<T>& orig) {
    this->tamf = orig.tamf;
    this->taml = orig.taml;
    this->vector = new T[tamf];
    for (int i = 0; i < this->taml; i++) {
        this->vector[i] = orig.vector[i];
    }
}

/**
 * @brief Constructor de copia parcial con posición inicial y número de elementos
 * @param orig VDinamico original a copiar
 * @param posicionInicial Posición inicial de copia
 * @param numElementos Número de elementos a copiar
 */

template<class T>
VDinamico<T>::VDinamico(const VDinamico<T>& orig, unsigned int posicionInicial, unsigned int numElementos) {
    if (posicionInicial > taml || numElementos > taml) throw std::out_of_range("El posicionInicial o numero es mayor que el vector");
    tamf = orig.tamf;
    taml = numElementos;
    // Redimensionamos tamf si es necesario, asi el tamaño fisico sera la potencia de 2 justo superior al tamaño logico
    while (taml <= tamf / 3) {
        tamf /= 2;
    }
    try {
        vector = new T[tamf];
    } catch (const std::bad_alloc&) {
        throw std::runtime_error("No hay memoria");
    }
    for (unsigned i = 0; i < taml; i++) {
        vector[i] = orig.vector[i + posicionInicial];
    }
}

/**
 * @brief Destructor de la clase, libera la memoria asignada al vector
 */
template<class T>
VDinamico<T>::~VDinamico() {
    delete[] vector;
}

/**
 * @brief Operador de asignación de copia
 * @param orig VDinamico original a copiar
 * @return Una referencia al objeto VDinamico actual después de la copia
 */
template<class T>
VDinamico<T>& VDinamico<T>::operator=(const VDinamico<T>& orig) {
    if (this != &orig) {
        delete[] vector;
        this->tamf = orig.tamf;
        this->taml = orig.taml;
        this->vector = new T[tamf];
        for (int i = 0; i < taml; i++) {
            this->vector[i] = orig.vector[i];
        }
    }
    return *this;
}

/**
 * @brief Operador de acceso al índice
 * @param pos Posición del elemento a acceder
 * @return Referencia al elemento en la posición 'pos'
 * @throws std::out_of_range Si 'pos' está fuera del rango válido
 */
template<class T>
T& VDinamico<T>::operator[](int pos) {
    if (pos < 0 || pos >= taml) throw std::out_of_range("Indice fuera del rango de posiciones del vector");
    return vector[pos];
}

/**
 * @brief Inserta un elemento en la posición dada
 * @param dato Elemento a insertar
 * @param pos Posición en la que se insertará el elemento
 */
template<class T>
void VDinamico<T>::insertar(const T& dato, unsigned int pos) {
    if (pos >= tamf && pos != UINT_MAX) throw std::out_of_range("La posicion es mayor al tamaño del vector");
    if (pos == UINT_MAX) {
        // No se ha especificado posicion, insertamos al final
        if (taml == 0) {
            // El vector esta vacio
            vector[taml] = dato;
        } else {
            pos = taml - 1;
            vector[pos] = dato;
        }
    } else if (pos < taml) {
        // Desplazo todos los valores de las posiciones superiores a la derecha para poder insertar el nuevo dato
        for (unsigned i = taml - 1; i >= pos; i--) {
            vector[i + 1] = vector[i];
        }
        vector[pos] = dato;
    }
    taml++;

    // Tras insertar, aumento el tamaño fisico si es necesario
    if (taml == tamf) {
        tamf *= 2;
        T* vaux;
        try {
            vaux = new T[tamf];
        } catch (std::bad_alloc& e) {
            throw std::runtime_error("No hay memoria");
        }
        for (unsigned i = 0; i < taml; i++) {
            vaux[i] = vector[i];
        }
        delete[] vector;
        vector = vaux;
    }
}

/**
 * @brief Borra un elemento en la posición dada
 * @param pos Posición del elemento a borrar
 * @return El elemento borrado
 */
template<class T>
T VDinamico<T>::borrar(unsigned int pos) {
    if (pos >= taml && pos != UINT_MAX) throw std::out_of_range("La posicion es mayor al tamaño del vector");

    T elementoEliminado;
    if (pos == UINT_MAX) {
        // Borrar ultima posicion
        elementoEliminado = vector[taml-1];
    } else if (pos < taml) {
        elementoEliminado = vector[pos];
        for (unsigned i = pos; i < taml; i++) {
            vector[i] = vector[i + 1];
        }
    }
    taml--;

    // Reducimos el tamaño fisico del vector si es necesario
    if (taml <= tamf / 3) {
        tamf /= 2;
        T* vaux;
        vaux = new T[tamf];
        for (unsigned i = 0; i < taml; i++) {
            vaux[i] = vector[i];
        }
        delete[] vector;
        vector = vaux;
    }
    return elementoEliminado;
}

/**
 * @brief Ordena el vector en orden ascendente
 */

template<class T>
void VDinamico<T>::ordenar() {
    sort(vector, vector + taml);
}

/**
 * @brief Ordena el vector en orden descendente
 */
template<class T>
void VDinamico<T>::ordenarRev() {
    sort(vector, vector + taml, greater<T>());
}

/**
 * @brief Realiza una búsqueda binaria en el vector
 * @param dato Elemento a buscar
 * @return La posición del elemento encontrado o -1 si no se encuentra
 */
template<class T>
int VDinamico<T>::busquedaBin(T& dato) {

    int izq = 0;
    int drcha = this->taml - 1;
    int centro = this->taml / 2;

    while (izq <= drcha) {
        centro = (drcha + izq) / 2;
        if (vector[centro] == dato) {
            return centro;
        } else {
            if (dato < vector[centro]) {
                drcha = centro - 1;
            } else {
                izq = centro + 1;
            }
        }
    }
    return -1;
}

/**
 * @brief Obtiene el tamaño lógico del vector
 * @return El tamaño lógico del vector
 */
template<class T>
unsigned VDinamico<T>::tamLogico() {
    return this->taml;
}

/**
 * @brief Imprime los primeros 'tam' elementos del vector en la consola
 * @param tam Cantidad de elementos a imprimir
 */
template<class T>
void VDinamico<T>::print(int tam ) {
    for( int i=0; i<tam; i++ ){
        cout << "Numero " << i << ":" << endl;
        cout << "\tID: " << vector[i].getId() << ",";
        cout << "\tIdent: " << vector[i].getIdent() << ",";
        cout << "\tTipo: " << vector[i].getTipo() << ",";
        cout << "\tNombre: " << vector[i].getNombre() << ",";
        cout << "\tLatitud: " << vector[i].getPosicion().getLatitud() << ",";
        cout << "\tLongitud: " << vector[i].getPosicion().getLongitud() << ",";
        cout << "\tContinente: " << vector[i].getContinente();
        cout << "\tIsoPais: " << vector[i].getIsoPais() << endl;
    }
}
;
#endif /* VECTORDINAMICO_H */