//
// Created by Dalton Gabriel Abambari on 25/9/23.
//

#include "Aeropuerto.h"

using namespace std;

Aeropuerto::Aeropuerto() = default;

Aeropuerto::Aeropuerto
(
    const string &id,
    const string &ident,
    const string &tipo,
    const string &nombre,
    const UTM &posicion,
    const string &continente,
    const string &isoPais
): _id(id), _ident(ident), _tipo(tipo), _nombre(nombre), _posicion(posicion), _continente(continente), _iso_pais(isoPais) {
    _id = id;
    _ident = ident;
    _tipo = tipo;
    _nombre = nombre;
    _posicion = posicion;
    _continente = continente;
    _iso_pais = isoPais;
}

Aeropuerto::Aeropuerto(const Aeropuerto &orig) {
    this->_id = orig._id;
    this->_ident = orig._ident;
    this->_tipo = orig._tipo;
    this->_nombre = orig._nombre;
    this->_posicion = orig._posicion;
    this->_continente = orig._continente;
    this->_iso_pais = orig._iso_pais;
}

//Aeropuerto &Aeropuerto::operator=(const Aeropuerto &f) = default;


Aeropuerto::~Aeropuerto() {

}

// Getters and Setters
const string &Aeropuerto::getId() const {
    return _id;
}

void Aeropuerto::setId(const string &id) {
    _id = id;
}

const string &Aeropuerto::getIdent() const {
    return _ident;
}

void Aeropuerto::setIdent(const string &ident) {
    _ident = ident;
}

const string &Aeropuerto::getTipo() const {
    return _tipo;
}

void Aeropuerto::setTipo(const string &tipo) {
    _tipo = tipo;
}

const string &Aeropuerto::getNombre() const {
    return _nombre;
}

void Aeropuerto::setNombre(const string &nombre) {
    _nombre = nombre;
}

const UTM &Aeropuerto::getPosicion() const {
    return _posicion;
}

void Aeropuerto::setPosicion(const UTM &posicion) {
    _posicion = posicion;
}

const string &Aeropuerto::getContinente() const {
    return _continente;
}

void Aeropuerto::setContinente(const string &continente) {
    _continente = continente;
}

const string &Aeropuerto::getIsoPais() const {
    return _iso_pais;
}

void Aeropuerto::setIsoPais(const string &isoPais) {
    _iso_pais = isoPais;
}


